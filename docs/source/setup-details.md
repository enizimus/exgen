# Setup property details 

:::{note}
This documentation will be updated each time a new release is available so please check which version of the library you are using as the feature list might be different. 
:::

Here are all of the details listed regarding the individual pieces of the library. 

## Config (*yaml*) file details

The config `.yaml` file has the following properties : 

| Property    | Type     | Description | Options |
| ----------- | -------- | ----------- | ------- |
| `template`  | String   | Which latex template to use. | homework |
| `tasks_template` | String | Name of file containing tasks latex text. | - |
| `figure_path` | String | Path to folder where to save generated figures. | - |
| `course` | String | Name of the course (shown in generated PDF). | - |
| `semester` | String | Semester id (shown in generated PDF). | - |
| `session_id` | Int | Exercise number (shown in generated PDF). | - |
| `session_name` | String | Exercise name (shown in generated PDF). | - |
| `groups` | List[String] | Group definitions (shown in generated PDF). | [Group syntax](#groups-syntax) |
| `seed_num` | Int | Random seed number. | - |
| `generate_solution_files` | Bool | Should solution files be generated? | True, False |
| `tasks` | List[Dict] | Contains one dictionary per task. | [Tasks syntax](#tasks-syntax) |

### Tasks syntax
The tasks property of the config file does not have any direct properties of its own but it is a list generated using standard *yaml* list syntax : 
```yaml
tasks: 
  - Dict1
  - Dict2
```
Here `Dict1` and `Dict2` are dictionaries which describe the individual tasks. These are filled with properties which are described below and used to generate the tasks. A dictionary in *yaml* files can be created by using curly brackets `{}` and using the `key:value` syntax, like `{key1:value1, key2:value2}`.

| Property    | Type     | Description | Options |
| ----------- | -------- | ----------- | ------- |
| `task_solver`  | String   | Which predefined solver to use for this task. | `resistor-network`, `electric-field-1`, `electric-field-2`, `electric-field-mpq-1`, `norton-equivalent`, `norton-thevenin-equivalent`, `helmholtz-1V-2I`, `thevenin-helmholtz-1V-1I`, `nodal-analysis` |
| `netlist_pool`  | List[String]   | Contains all netlists which can be used for this task. | - |
| `solution_gen_type`  | String   | How to generate results file. | - |
| `round_precision`  | Int   | Rounding precision. | - |
| `var_defs`  | Dict[String, Any]   | Variable definition dictionary. | [Variable definition syntax](#variable-definition-syntax) |

### Groups syntax
The groups can be created by using the following syntax. It is important that the `groups` property is a list containing strings. The individual strings consist of two main parts : 
- The name of the group
- The identifying number of the group

One could write out the names of the groups directly like `["BME1", "BME2", ..., "BME9"]` but to make it easier on the eyes you can just use the name and id syntax. For generating the same list as in the previous example we can write `["BME:1-9"]` and this will generate all of the 9 groups. Additional group names can be added to the same list `["BME:1-9", "ETDETONI:1-7", "ICE2"]`. As you can see it is also possible to additionally define groups without the name-id syntax.

### Variable definition syntax
The `var_defs` property is a dictionary mapping from strings to any object. In the yaml file we can use the standard `{key:value}` syntax to construct it. The keys of the `var_defs` dictionary are the variable names used in the task and the corresponding values are dictionaries describing how to generate these. So any value which we want to define as variable we can add it to the `var_defs`. To this end each entry of the dictionary has to have the following setup : 
```yaml
{"varname": vardef}
```
where `vardef` is itself a dictionary which can take on two forms : 
1. We want to fix the values of the variable : 
   ```yaml
   # here we define a variable with fixed value of 10.0
   {"dist": "exact", "val": 10.0}
   ```
2. We want to draw the value from a distribution : 
   ```yaml
   # here we define a variable whose value is drawn from an uniform 
   # distribution within the range (100, 500) and it is rounded to 
   # an integer value
   {"dist": "uniform", "min": 100, "max": 500, "round": True, "int": True}
   ```

Putting this together we could generate a `var_defs` dictionary for three variables $R_1$, $R_2$ and $U_q$ like : 
```yaml
{"R1": {"dist": "exact", "val": 100.0},
 "R2": {"dist": "uniform", "min": 200, "max": 300, "round": True, "int": True}, 
 "Uq": {"dist": "exact", "val": 5.2}}
```

## Tasks (*tex*) file details

The *tasks.tex* file contains the text which is displayed around a task. This text describes the task and sets up how the final task will look like when rendered to a PDF file. It consists of parts which are separated by `%END-PART`, which is always located at the end of the part. The first part is always the *info-box*. Currently it is implemented such that the first part is always the *info-box* so if the task does not have one just add a `%END-PART` and continue with the task definitions. The following parts are the task definitions which are also separated with the `%END-PART` separator. 

To allow flexibility while creating tasks it is possible to define *variables* in the *tasks.tex* file which are then replaced with actual values. To define a variable in this context we just need to write `INSERT-VARNAME` where `VARNAME` can be for example `R1` or any other variable which is defined in the [`var_defs`](#variable-definition-syntax) dictionary in the config file. We can also define variables for results which are calculated within the solver function. 

:::{note}
To successfully use the `INSERT-VARNAME` syntax, `VARNAME` has to be the same as the string used to store the variable in either the *variables* or *result* dictionaries.
:::

Furthermore it is possible to enable certain parts of the text in *tasks.tex* if the group for which it is being created is selected. For example, we have a subtask d) which is only meant for BME groups. We can just add the following line : 

```tex
%INSERT-BME d) This task is about...
```

The library will just remove the `%INSERT-BME` and with this uncomment this line in the case of a BME group. With a similar syntax we can also make the correct point system between groups with different subtasks. An example would be : 

```tex
\item[(a)] Subtask 1 text... %INSERT-NOTBME\textbf{(1.5 Punkte)}\\ 
%INSERT-BME\textbf{(1 Punkt)}\\
\item[(b)] Subtask 2 text... %INSERT-NOTBME\textbf{(1.5 Punkte)}\\ 
%INSERT-BME\textbf{(1 Punkt)}\\
```

This would insert the correct points depending on the group in question. Finally by placing `INSERT-SOLUTION` at the end of a task part we can set where in the *solution-file* the solution is inserted. This is a part that might be changed in the future to not be needed at all but now it is like this.

## Netlist details

:::{note}
Install the lcapy library and you can test any of the netlists by looking up the usage in the [lcapy](https://lcapy.readthedocs.io/en/latest/overview.html#netlist-schematics) netlist documentation. 
:::

The netlists used here follow a syntax similar to the *tikz* library. Here it is even a bit more simple. More information can be found in the documentation of the [lcapy](https://lcapy.readthedocs.io/en/latest/index.html) python library to which the netlists are fed. 

Consider the following example : 

```
V0  0 _1; down=1.5, v=U_{q1}, l=
R1 _1 _2; down=1.5, v=U_{R1}, i=I_{R1}
R2 _2 _3; right=1.5, v^>=U_{R1}, i=I_{R1}
W  _3 _4; up
W  _4  0; left
; label_nodes=false, label_values=false
```

This netlist will give the following netlist : 

```{figure} ./figs/simple_netlist.png
:scale: 50 %
:alt: simple-netlist-network

Network generated from the netlist given above.
```

The netlist is built in such a way that each line describes one element in the network. The type of element can be any electrical component including : 

- Resistor : `R`
- Inductor : `L`
- Capacitor : `C`
- Voltage source : `V`
- Current source : `I`
- Wire : `W`

The number by the component designator is a id of the component type and when later simulating the network we can refer to the components by the combination of element letter and number (for example : resistor three would be `R3`). After the component definition we specify the two nodes to which the component is connected. 
```
V0  0 _1; down=1.5, v=U_{q1}, l=
```
Here we have a voltage source `V0` between the nodes `0` and `_1`. It is important that each network has the node `0` defined somewhere, if possible where the ground is. The underscore before the 1 is just used to tell the program when generating the network image to not draw a node at the node location. As can be seen in the network above only one node has a black dot drawn and this is the `0` node. 

This part ends with a semicolon `;`. To understand the next part first understand that the drawing of the network happens on a cartesian grid. The location of where each node is placed depends on the first node - the origin. So by placing `0` somewhere we can think of it as being at $(0,0)$. Then for each element by specifying the direction with `up`, `down`, `left` or `right`, we tell it where from the starting node to place the end node of the component. Further we can specify how much in this direction we want to go, `down=1.5` would set the node `_1` at  $(0, -1.5)$. This way one can draw the whole network just by connecting the nodes. 

:::{note}
A good approach is to first draw the network by hand and label all of the nodes in the network and then just transfer the network with the given syntax into a netlist.
:::

The rest of the properties are mainly used for labeling the component. Here we have three main labels : 

- Voltage label `v=U_{q1}` : If given the drawn network will have an arrow representing the voltage over the component with the given voltage name. 
- Current label `i=I_{q1}` : Same as the voltage label.
- Component name label `l=` : If set to empty the component won't show any name label meaning that the resistors would have no $R_1$ written beside them.

It is further possible to set the direction and location of the voltage and current arrows and labels by using `^,_,>,<` before the `=` sign.

The final line :

```
; label_nodes=false, label_values=false
```

is just there to set up the behavior while drawing the network. 

- `label_nodes=false` means that the node names are not drawn. We can set it to `true` if we want all of them to be drawn of if only nodes with a name beginning with a letter should be drawn the set it to `alpha`.
- `label_values=false` means that the values of the components are not drawn onto the network itself. 
