# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'EXgen'
copyright = '2023, Eniz M.'
author = 'Eniz M.'
release = '0.1.7.'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.duration', 
              'myst_parser',
              ]

templates_path = ['_templates']
exclude_patterns = []

myst_enable_extensions = ['colon_fence', 'dollarmath', 'amsmath']
myst_heading_anchors = 3
myst_dmath_allow_labels=True
myst_dmath_double_inline = True

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_book_theme'
html_static_path = ['_static']
