# Installation 

To successfully generate exercise materials and use the library you will
need to install the following dependencies. 

## Environment setup 

This section covers the setup of the environment used for running the library. Please make sure that you follow the instructions in the given order. 

:::{important}
If you are a developer you still need to perform this setup process. When you have successfully completed it then follow the developer setup instructions.
:::

1. Install python version 3.9. It is recommended to install [anaconda](https://www.anaconda.com/download) or [miniconda](https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html). After doing so, open the command prompt and follow the next steps.
   - Create a new environment with : 
   ```bash
    conda create --name exgen python=3.9
    ```
   - Switch to new environment with : 
    ```bash
    conda activate exgen
    ```
2. Install Latex 
   - For linux users install *texlive* : 
    ```bash
    sudo apt install texlive-latex-extra
    ```
   - For windows install [MiKTeX](https://miktex.org/howto/download-miktex)
   - after installation open command prompt and run `pdflatex --version` if it outputs the version of current install then you are set.
3. Install *EXgen* 
   - If you are using conda as suggested just run :
    ```bash
    python -m pip install --upgrade exgen
    ```
   - If you are using your system python :
    ```bash
    pip install --upgrade exgen
    ```
4. Test install 
   - Run the following in your command line 
    ```bash
    exgen --help
    ```
   - This should be returned : 
    ```bash
    usage: exgen [-h] cfg

    Welcome to EXgen : This command line interface will assist you in generating your exercise materials. Provide the name of the configuration yaml file and the generation
    will be done.

    positional arguments:
      cfg         Configuration file name.

    optional arguments:
      -h, --help  show this help message and exit
    ```

## Developer install 

:::{important}
If you haven't, please first complete the [*Environment setup*](#environment-setup) step.
:::

As a developer you will need to install a few dependencies more than what is needed to just use the package. 

1. Install documentation specific deps : 
   ```
   pip install --upgrade sphinx myst-parser sphinx-book-theme
   ```
2. Install the requirements by running : 
   ```bash
   python -m pip install -r requirements.txt
   ```

### Build and update on PyPi 

```bash
# building the new release : 
python -m build

# upload to pypi : 
python -m twine upload --skip-existing dist/*
```
