# EXgen : 

Python library for automatically generating Exercise or Exam sheets from configuration files. For more information please visit the [EXgen documentation](https://enizimus.gitlab.io/exgen).