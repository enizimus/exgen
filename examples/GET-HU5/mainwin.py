from EXgen import EXgen
from ypstruct import structure

def main() -> None:
    args = structure()
    args.config_file = "UE_config.yaml"
    myGen = EXgen(args)
    myGen.generate()
    print("finished")

if __name__ == "__main__":
    main()