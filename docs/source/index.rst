.. EXgen documentation master file, created by
   sphinx-quickstart on Wed Nov  8 13:15:06 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EXgen's documentation!
=================================

.. note::

   This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   getting-started
   setup-details

