# Getting started

The idea of this package is to allow the automated generation of exercise materials without having to code. 

## File structure 

Because no code needs to be written for the generation of the exercise materials the configuration information needs to be stored somewhere. Therefore we have to define 2 files for each project (In this context a *project* denotes the exercise that we want to generate). The main two files are : 

- `config.yaml` : contains the configuration information. 
- `tasks.tex` : contains latex sections for the specific exercise tasks.

## Simple example 

Here we will take a look at one example where the exercise 3 for GET UE is generated. For this we run the following command which will set up an example project : 

```bash
exgen --quickstart exgen-example
```

After finishing a directory with the name `exgen-example` will be generated. In it we will find a `config.yaml`, `tasks.tex` file and a `nets` directory containing netlist files. After changing to this directory we can run : 

```bash
exgen --generate config.yaml
```

This will run the generation process and when finished the generated pdf files will be moved to a `generated-files` directory.

For a detailed explanation of the `yaml` and `tex` files please see below.

### Exgen-example : `config.yaml`

:::{note}
For a detailed summary of all `config.yaml` properties please check out the [configuration file details](./setup-details.md#config-yaml-file-details). 
:::

The configuration file starts with the following lines :

```yaml
# UE 3 config file :

# General setup : 
template : "homework" 
tasks_template : "tasks.tex"
figure_path : "task_figures"

# main meta data : 
course : "GET UE"
semester : "WS 23/24"
session_id : 3
session_name : "Ersatzquellenverfahren und Leistungsanpassung"
groups : ["BME:1-3", "ICE1"]
seed_num : 42

generate_solution_files: True
```

Here, the meta data of the whole project is set up. This includes the choice of the template, defining the tasks `tex` file, course name and so on. The groups list will dictate how many pdf files are generated in the end. The next part of the configuration file contains information about the setup of the tasks that we want in the exercise. 

```yaml
# tasks setup : 
tasks:
   - { task_solver : norton-equivalent,       
       netlist_pool : ["./nets/schematic_1_1.net",
                       "./nets/schematic_1_2.net",
                       "./nets/schematic_1_3.net",
                       "./nets/schematic_1_4.net"],
       round_precision : 1,
       var_defs : {"R1": {"min": 100, "max": 500, "dist": "uniform", "round": True, "int": True}, 
                   "R2": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "R3": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "R4": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "R5": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "RL": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
             "V0": {"min": 1, "max": 10, "dist": "uniform", "round": True},},
       solution_gen_type : list-results
   }
   
   - { task_solver : norton-thevenin-equivalent,       
       netlist_pool : ["./nets/schematic_2_1.net",
                       "./nets/schematic_2_2.net",
                       "./nets/schematic_2_3.net",
                       "./nets/schematic_2_4.net"],
       round_precision : 1,
       var_defs : {"R1": {"min": 100, "max": 500, "dist": "uniform", "round": True, "int": True}, 
                   "R2": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "R3": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "R4": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "R5": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "RL": {"min": 100, "max": 200, "dist": "uniform", "round": True, "int": True},
                   "I0": {"min": 1, "max": 10, "dist": "uniform", "round": True},},
       solution_gen_type : list-results
   }
```

Here we define a tasks list in the yaml file with two elements - two tasks. Each task is defined as a dictionary containing the solver information, netlist pool (a list of possible networks for the task from which one is randomly selected per group) and variable distribution definitions (from which the values of the variables are drawn).

### Exgen-example : `tasks.tex`

:::{note}
For a detailed summary of all `config.yaml` properties please check out the [configuration file details](./setup-details.md#tasks-tex-file-details). 
:::

This tex file contains all of the text which goes into the exercise tasks. The latex code itself is filled with placeholders for values which are inserted by the library within the generation process.

